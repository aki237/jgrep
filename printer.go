package main

import (
	"fmt"
	"strconv"
	"strings"
)

// Printer struct is used to store all data for formatted printing of the query output
type Printer struct {
	data   []string
	format string
}

// NewPrinter is used to create a new Printer object
func NewPrinter(format string) *Printer {
	return &Printer{data: make([]string, 0), format: format}
}

// Commit method is used to print the contents in data field according to the format passed
func (p *Printer) Commit() {
	if p.format == "" {
		for _, value := range p.data {
			fmt.Println(value)
		}
		p.data = make([]string, 0)
		return
	}

	// Replace some passed escape sequences with actual ones
	form := strings.Replace(strings.Replace(p.format, "\\n", "\n", -1), "\\t", "\t", -1)
	finalStr := ""
	splits := strings.Split(form, "#{")
	for _, val := range splits {
		spl2 := strings.SplitN(val, "}", 2)
		if len(spl2) == 1 {
			finalStr += spl2[0]
			continue
		}

		// TODO: Add some more post processing like base64 encoding and decoding
		num, err := strconv.Atoi(spl2[0])
		if err != nil {
			if !strings.HasPrefix(spl2[0], "@") {
				finalStr += "#{" + val
				continue
			}
			sep := " "
			if len(spl2[0]) > 1 {
				sep = spl2[0][1:]
			}
			finalStr += strings.Join(p.data, sep) + spl2[1]
			continue
		}

		if num < 1 || num > len(p.data) {
			finalStr += "#{" + val
			continue
		}

		finalStr += p.data[num-1]
		finalStr += spl2[1]
	}

	fmt.Println(finalStr)
	p.data = make([]string, 0)
}

// Store method is used to store an output to the data queue
func (p *Printer) Store(content interface{}) {
	p.data = append(p.data, fmt.Sprint(content))
}
