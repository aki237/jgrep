package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

var (
	errNotFound = "object not found : "
)

var printer *Printer

type queryList []string

func (i *queryList) String() string {
	return "my string representation"
}

func (i *queryList) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: jgrep -q QUERY [options...]\n")
	fmt.Fprintf(os.Stderr, "\nFlags: \n")
	fmt.Fprintf(os.Stderr, "\t-%s\t%s\n", "q", "List of queries to be performed [needed]")
	fmt.Fprintf(os.Stderr, "\t-%s\t%s\n", "f", "Filename to perform query with (Default : /dev/stdin)")
	fmt.Fprintf(os.Stderr, "\t-%s\t%s\n", "p", "Format to print the output")
}

func main() {
	var ql = make(queryList, 0)
	var filename = ""
	var template = ""
	flag.Var(&ql, "q", "List of queries to be performed")
	flag.StringVar(&filename, "f", "/dev/stdin", "Filename to perform query with")
	flag.StringVar(&template, "p", "", "Format to print the output")
	flag.Usage = usage
	flag.Parse()

	if len(ql) == 0 {
		flag.Usage()
		return
	}

	printer = NewPrinter(template)

	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		return
	}

	var nxx int
	var root JSONObject
	backlog := string(bs)
	for nxx != -1 {
		line := backlog

		root, nxx, err = GETJSONObject(line)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error Here : %s\n", err)
			return
		}

		if nxx > 0 {
			backlog = backlog[nxx:]
		}

		for _, q := range ql {
			obj := root
			splits := removeEmpties(strings.Split(q, "."))
			for i, val := range splits {
				if obj == nil {
					continue
				}
				obj, err = obj.Selection(val)
				if err != nil {
					fmt.Fprintf(os.Stderr, "%s\n", err)
					return
				}
				if obj == nil && i < len(splits)-1 {
					if !strings.HasSuffix(val, "?") {
						fmt.Fprintf(os.Stderr, "cannot propagate further in a terminal object %s\n", val)
						return
					}
					continue
				}
			}
			if obj != nil {
				printer.Store(obj)
			}
		}
		printer.Commit()
	}
}
