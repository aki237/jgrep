# jgrep

**jgrep** is a simple commandline json processor in golang.

## Installation

```shell
$ go get gitlab.com/aki237/jgrep
```

Make sure that `GOPATH` is in your `PATH`.

## Usage

For usage, let's look at some examples :

### Example 1 : `Github API`

Let's get the latest 30 commits from the [`torvalds/linux`](https://github.com/torvalds/linux) and put it into a file :

```
$ curl https://api.github.com/repos/torvalds/linux/commits > commit.json
```

From here I'm going to reformat the needed contents into `git log` format :

```shell
$ jgrep -f commit.json -q *. \
          | jgrep \
		            -q "sha" \
					-q "commit.author.name" \
					-q "commit.author.email" \
					-q "commit.author.date" \
					-q "commit.message" \
					-p "commit #{1}\nAuthor: #{2} <#{3}>\nDate: #{4}\n\n#{5}\n\n"
```

[*Output*](https://aki237.me/output.txt)

In the first line, I'm extracting the objects from an array (take a look at the content from the URL).
In the next pipe, I'm providing 5 queries which extract SHAsum of commit, Name of the committer, Email of the Committer,
Date of Commit, Commit message. These values will be stored in `#{1}`, `#{2}` and etc., (till `${5}`). In the `-p` flag
I'm providing the format to print the content as. That must be self explainatory.

### Example 2 : `echo`

```
$ echo "[1,2,3,4,5,6]" | jgrep -q "*." -p "#{@+}"
1+2+3+4+5+6
```

Simple as before, `#{@}` is a special delimiter to specify all variables to printed jointly. The string following `@` before `}`
will be taken as the separator. In this case `+`. Just so we can do this :

```shell
$ echo "[1,2,3,4,5,6]" | jgrep -q "*." -p "#{@+}" | bc
21
```

Any issues or improvements are welcome.

## Plans
 + Add some extra post processing techniques like `base64` encoding and decoding, `shaXXX` hashing etc.,
 + Possibly syntax highlighting for processes JSON Output
