package main

// JSONObject is an interface denoting a valid Json field
type JSONObject interface {
	Selection(query string) (JSONObject, error)
}
