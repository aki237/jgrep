package main

import (
	"encoding/json"
	"errors"
	"strconv"
)

// Array type is used to denote the JSON Arrays
type Array []interface{}

// Selection method satisfies the JSONObject interface.
// This returns a JSONObject for the given query string
func (a Array) Selection(query string) (JSONObject, error) {
	if a == nil {
		return nil, errors.New("null parent object : " + query)
	}

	if query == "*" {
		ret := make(ResultArray, 0)
		for _, valarray := range a {
			switch t := valarray.(type) {
			case map[string]interface{}:
				ret = append(ret, Struct(t))
			case []interface{}:
				ret = append(ret, Array(t))
			case int64, float64, string, bool, nil:
				printer.Store(t)
			}
		}
		if len(ret) == 0 {
			return nil, nil
		}
		return ret, nil
	}

	num, err := strconv.Atoi(query)
	if err != nil {
		return nil, errors.New("needs a integer as an array index : " + query)
	}

	if num >= len(a) {
		return nil, errors.New("index number greater than array index : " + query)
	}

	if num < 0 {
		return nil, errors.New("index number less than 0 : " + query)
	}

	object := a[num]
	switch t := object.(type) {
	case map[string]interface{}:
		return Struct(t), nil
	case []interface{}:
		return Array(t), nil
	case int64, float64, string, bool, nil:
		printer.Store(t)
	}
	return nil, nil
}

// String method provides the stringer interface to facilitate printing (fmt.Println)
func (a Array) String() string {
	content, _ := json.MarshalIndent(&a, "", "  ")
	return string(content)
}

// InitFromArray function returns a Array object by parsing the passed json string content
func InitFromArray(jsonStr string) (JSONObject, error) {
	cont := make(Array, 0)

	err := json.Unmarshal([]byte(jsonStr), &cont)
	if err != nil {
		return nil, err
	}

	return cont, nil
}
