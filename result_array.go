package main

import "encoding/json"

// ResultArray type denotes a array of results from previous queries.
// A Selection call on this will always yield a nil object or a ResultArray,
// as this is supposed to print the final result from all the elements.
type ResultArray []JSONObject

// Selection method satisfies the JSONObject interface.
// This returns a JSONObject for the given query string
func (r ResultArray) Selection(query string) (JSONObject, error) {
	ret := make(ResultArray, 0)

	for _, val := range r {
		jo, err := val.Selection(query)
		if err != nil {
			return nil, err
		}

		if jo == nil {
			continue
		}

		ret = append(ret, jo)
	}

	if len(ret) == 0 {
		return nil, nil
	}

	return ret, nil
}

// String method provides the stringer interface to facilitate printing (fmt.Println)
func (r ResultArray) String() string {
	content := ""
	for i, val := range r {
		part, _ := json.MarshalIndent(val, "", "  ")
		content += string(part)
		if i != len(r)-1 {
			content += "\n"
		}
	}
	return content
}
