package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

// GETJSONObject function is used to get the right JSONObject for the given json string.
// The type of JSONObject is decided on the leading character/token of the JSON string sent.
// The returns will be of an Array type if the token is '[' or an Struct if it is '{'.
// Otherwise an error will be returned.
func GETJSONObject(line string) (JSONObject, int, error) {
	var jobj JSONObject
	var err error
	switch true {
	case strings.HasPrefix(strings.TrimSpace(line), "["):
		jobj, err = InitFromArray(line)
	case strings.HasPrefix(strings.TrimSpace(line), "{"):
		jobj, err = InitFromStruct(line)
	default:
		return nil, -1, fmt.Errorf("not a valid json object")
	}

	if err == nil {
		return jobj, -1, err
	}

	synerr, ok := err.(*json.SyntaxError)
	if ok {
		jobj, _, err = GETJSONObject(line[:synerr.Offset-1])
		return jobj, int(synerr.Offset - 1), err
	}

	return jobj, -1, err
}

func removeEmpties(strs []string) []string {
	x := make([]string, 0)
	for _, val := range strs {
		if val != "" {
			x = append(x, val)
		}
	}
	return x
}
