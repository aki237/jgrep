package main

import (
	"encoding/json"
	"errors"
	"strings"
)

// Struct type is used denote a JSON Object
type Struct map[string]interface{}

// Selection method satisfies the JSONObject interface.
// This returns a JSONObject for the given query string
func (c Struct) Selection(query string) (JSONObject, error) {
	if c == nil {
		return nil, errors.New("null parent object : " + query)
	}

	object, found := c[query]
	if !found {
		if !strings.HasSuffix(query, "?") {
			return nil, errors.New(errNotFound + query)
		}
		query = query[:len(query)-1]
		object, found = c[query]
		if !found {
			return nil, nil
		}
	}

	switch t := object.(type) {
	case map[string]interface{}:
		return Struct(t), nil
	case []interface{}:
		return Array(t), nil
	case int64, float64, string, bool, nil:
		printer.Store(t)
	}
	return nil, nil
}

// String method provides the stringer interface to facilitate printing (fmt.Println)
func (c Struct) String() string {
	content, _ := json.MarshalIndent(&c, "", "  ")
	return string(content)
}

// InitFromStruct function returns a Struct object by parsing the passed json string content
func InitFromStruct(jsonStr string) (JSONObject, error) {
	cont := make(Struct)

	err := json.Unmarshal([]byte(jsonStr), &cont)
	if err != nil {
		return nil, err
	}

	return cont, nil
}
